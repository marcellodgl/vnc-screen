# SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
# SPDX-License-Identifier: BSD-2-Clause

cmake_minimum_required(VERSION 3.14)

# KDE Application Version, managed by release script
set (RELEASE_SERVICE_VERSION_MAJOR "0")
set (RELEASE_SERVICE_VERSION_MINOR "03")
set (RELEASE_SERVICE_VERSION_MICRO "04")
set (RELEASE_SERVICE_VERSION "${RELEASE_SERVICE_VERSION_MAJOR}.${RELEASE_SERVICE_VERSION_MINOR}.${RELEASE_SERVICE_VERSION_MICRO}")

project(vncscreen VERSION ${RELEASE_SERVICE_VERSION})

set(QT_MIN_VERSION "5.15.0")
set(KF5_MIN_VERSION "5.76.0")

include(FeatureSummary)

################# set KDE specific information #################
find_package(ECM ${KF5_MIN_VERSION} REQUIRED NO_MODULE)

# where to look first for cmake modules, before ${CMAKE_ROOT}/Modules/ is checked
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH})
set(CMAKE_INCLUDE_CURRENT_DIR ON)

include(KDEInstallDirs)
include(KDECMakeSettings)
#include(KDECompilerSettings NO_POLICY_SCOPE)
include(ECMSetupVersion)
include(ECMGenerateHeaders)
include(ECMPoQmTools)
ecm_setup_version(${PROJECT_VERSION}
    VARIABLE_PREFIX VNCSCREEN
    SOVERSION ${PROJECT_VERSION_MAJOR}
    VERSION_HEADER "${CMAKE_CURRENT_BINARY_DIR}/src/config-vncscreen.h"
)

################# Find dependencies #################
find_package(Qt${QT_MAJOR_VERSION} ${QT_MIN_VERSION} REQUIRED COMPONENTS Core Gui Qml QuickControls2 Svg Sql)
find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS Kirigami2 I18n CoreAddons)
find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core Quick Gui Qml QuickControls2 Svg Sql)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core Quick)
find_library(VNCCLIENT vncclient)
if(NOT ANDROID)
    find_package(Qt${QT_MAJOR_VERSION} ${QT_MIN_VERSION} REQUIRED COMPONENTS DBus Widgets)
    find_package(KF5DocTools ${KF5_MIN_VERSION} REQUIRED)
else()
    file(ARCHIVE_EXTRACT INPUT ${CMAKE_SOURCE_DIR}/src/archive.libvncclient.tar.gz DESTINATION src)
endif()



################# build and install #################


add_subdirectory(src)


install(PROGRAMS org.kde.vncscreen.desktop DESTINATION ${KDE_INSTALL_APPDIR})
install(FILES org.kde.vncscreen.appdata.xml DESTINATION ${KDE_INSTALL_METAINFODIR})
install(FILES vncscreen.svg DESTINATION ${KDE_INSTALL_FULL_ICONDIR}/hicolor/scalable/apps)

ki18n_install(po)


feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)

if(ANDROID)
    kirigami_package_breeze_icons(ICONS
        go-home
        favorite
        help-symbolic
        help-about-symbolic
        applications-graphics
        color-picker
        reverse
        randomize
        network-connect
        network-disconnect
        preferences-system-windows-effect-screenshot
        help-about
        application-exit
        edit-clear-history
        dialog-ok
        input-keyboard-virtual
        input-mouse-click-left
        input-mouse-click-right
    )
endif()

file(GLOB_RECURSE ALL_CLANG_FORMAT_SOURCE_FILES src/*.cpp src/*.h)
