/*
VNC Screen: Remote Desktop application for Sailfish OS based on VNC standard.

Copyright (c) 2016 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#define APP_VERSION "0.3.0"
#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif
#include <QtQml>
#include <QQmlEngine>
#include <QScopedPointer>
#include <QQuickView>
#include <QGuiApplication>
#include "interfacerfb.h"
#include "interfacesettings.h"
#include "screenprovider.h"
#ifndef Q_OS_ANDROID
#include <QApplication>
#else
#include <QGuiApplication>
#endif
#include <QQmlApplicationEngine>
#include <QUrl>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <KAboutData>
#include <QDebug>
#include "config-vncscreen.h"
#include "about.h"
Q_DECL_EXPORT int main(int argc, char *argv[])
{

    qmlRegisterType<InterfaceRFB>("plasma.vncscreen.InterfaceRFB", 1, 0, "InterfaceRFB");
    qmlRegisterType<InterfaceSettings>("plasma.vncscreen.InterfaceSettings", 1, 0, "InterfaceSettings");
    //importante mettere i setSource dopo la configurazione degli engine


#if QT_VERSION < QT_VERSION_CHECK(6,0,0)
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
#ifndef Q_OS_ANDROID
    QApplication app(argc, argv);
#else
    QGuiApplication app(argc, argv);
#endif

    QIcon windowIcon = QIcon(":/Vncscreen/icons/vncscreen.svg");
    app.setWindowIcon(windowIcon);
    QCoreApplication::setOrganizationName(QStringLiteral("DGL"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("DGL"));
    QCoreApplication::setApplicationName(QStringLiteral("VNC Screen"));
    app.setApplicationDisplayName("Vnc Screen");
    QCoreApplication::setApplicationVersion(APP_VERSION);
    KLocalizedString::setApplicationDomain("vncscreen");
    qDebug()<<"available domain translations "<<KLocalizedString::availableDomainTranslations("vncscreen");
    qDebug()<<"italian "<<KLocalizedString::isApplicationTranslatedInto("it");

    qDebug()<<"application domain"<<KLocalizedString::applicationDomain();
    QQmlApplicationEngine engine;


    KLocalizedContext *context = new KLocalizedContext(&engine);
    context->setTranslationDomain("vncscreen");
    engine.rootContext()->setContextObject(context);


    qDebug()<<KLocalizedString::languages();

    KAboutData aboutData(
                // The program name used internally.
                QStringLiteral("vncscreen"),
                // A displayable program name string.
                i18nc("@title", "Vnc Screen"),
                // The program version string.
                QStringLiteral(VNCSCREEN_VERSION_STRING),
                // Short description of what the app does.
                i18n("VNC Remote desktop application"),
                // The license this code is released under.
                KAboutLicense::GPL,
                // Copyright Statement.
                i18n("(c) 2022"));
    aboutData.addAuthor(i18nc("@info:credit", "Marcello Di Guglielmo"), i18nc("@info:credit", "Author Role"), QStringLiteral("marcellodgl@aruba.it"), QStringLiteral(""));

    ScreenProvider *screenProvider= new ScreenProvider(QQmlImageProviderBase::Image);
    engine.addImageProvider(QLatin1String("rfbimage"),screenProvider);
    qmlRegisterSingletonType<AboutType>("plasma.vncscreen.About", 1, 0, "AboutType", [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject * {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)

        return new AboutType();
    });
    KAboutData::setApplicationData(aboutData);



    engine.load(QUrl(QStringLiteral("qrc:/qml/plasma-vncscreen.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }





    return app.exec();
}

