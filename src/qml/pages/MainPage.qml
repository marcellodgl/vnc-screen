/*
VNC Screen: Remote desktop application for Kirigami KDE based on VNC standard.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import QtQuick.Controls 2.5
import plasma.vncscreen.InterfaceRFB 1.0
import org.kde.kirigami 2.5 as Kirigami

Kirigami.Page {
    id: root
    signal vncPreviewUpdate(string source)
    //    property bool mouseActive: false
    enum Status{Connected, Disconnected}
    property int status: MainPage.Status.Disconnected
    //    allowedOrientations: Orientation.All
    title: i18nc("@title","VNC Screen")

    function connectScreen(value){
        if(value) connectSheet.open()
        else screenInterface.vncDisconnect()
    }
    function screenshot(){
        screenInterface.save()
            console.log("image saved on ")
    }

    InterfaceRFB{
        id: screenInterface
        onPasswordRequest: {
            console.log("* interfaceRFBPassword request")
            if(passwordSheet.connectionPassword==""){
                passwordSheet.open()
            }

        }
        onVncStatus: {
            status==InterfaceRFB.Connected ? root.status=MainPage.Status.Connected : {}
            status==InterfaceRFB.Connected ? keyboardAction.enabled=true : {}
            status==InterfaceRFB.Connected ? mouseLeftAction.enabled=true : {}
            status==InterfaceRFB.Connected ? mouseRightAction.enabled=true : {}
            status==InterfaceRFB.Connected ? flickableScreen.visible=true : {}
            status==InterfaceRFB.Disconnected ? root.status=MainPage.Status.Disconnected : {}
            status==InterfaceRFB.Disconnected ? keyboardAction.enabled=false : {}
            status==InterfaceRFB.Disconnected ? mouseLeftAction.enabled=false : {}
            status==InterfaceRFB.Disconnected ? mouseRightAction.enabled=false : {}
            status==InterfaceRFB.Disconnected ? passwordSheet.connectionPassword="" : {}
            status==InterfaceRFB.Disconnected ? imageScreen.source="" : {}
            status==InterfaceRFB.Disconnected ? flickableScreen.visible=false : {}
        }
        onVncImageUpdate: {
            //            console.log("image update"+index)
            running ? imageScreen.source="image://rfbimage/horizontal"+index : vncPreviewUpdate("image://rfbimage/horizontal"+index)
            console.log("image update complete"+index)


        }
        onResolutionChanged: {
            console.log("Resolution changed "+width+"x"+height)
            flickableScreen.contentWidth=width
            flickableScreen.contentHeight=height
            screenPinch.currentScale=1
        }
        onNotificationMessage: {
            showPassiveNotification(text,"short")
        }

        //viene fermato il refresh dello screen quando l'applicazione viene messa in background
        running: Qt.application.state === Qt.ApplicationActive
    }

    //L'utilizzo del component con innestato il dialog ha il ruolo di rendere il dialog come oggetto temporaneo che viene distrutto a fine utilizzo
    //in caso contrario si tratterebbe di un dialog che resta allocato e sarebbe sempre lo stesso nei successivi utilizzi
    //    Component{
    //        id:connectComponent
    ConnectPage{
        id : connectSheet
        onAccepted: {
            screenInterface.vncpath=connectionUrl
            screenInterface.vncquality=screenQuality
        }
    }
    //    }
    //    Component{
    //        id:passwordComponent
    PasswordPage{
        id : passwordSheet
        onAccepted: {
            console.log("connectionPassword: "+connectionPassword);
            screenInterface.vncpassword=connectionPassword
        }
    }

    //    }


    Flickable {
        anchors.fill: parent

        id: flickableScreen
        ScrollBar.horizontal: ScrollBar{
            //        active: true
            //        parent: root
            onPositionChanged: {
                console.log("position "+position+" stepsize "+stepSize+" minimumSize "+minimumSize+" size "+size+" flickableWidth "+flickableScreen.width+" flickableContentWidth "+flickableScreen.contentWidth)
            }
        }
        ScrollBar.vertical: ScrollBar{
        }
        //        onMovementEnded:   {
        //            //            console.log("flick x"+visibleArea.xPosition+"image x"+imageScreen.x)
        //            //                keyboardText.x=contentX
        //            //                keyboardText.y=contentY
        //        }



        PinchArea {
            property real initialWidth
            property real initialHeight
            property real currentScale: 1
            //            anchors.fill: parent
            id:screenPinch
            //            width: Math.max(root.contentWidth, root.width)
            //            height: Math.max(root.contentHeight, root.height)
            enabled: !(mouseLeftAction.checked || mouseRightAction.checked)
            width: flickableScreen.contentWidth
            height: flickableScreen.contentHeight

            Image {
                id:imageScreen
                fillMode: Image.PreserveAspectFit
                width: flickableScreen.contentWidth
                height: flickableScreen.contentHeight
                verticalAlignment: Image.AlignTop

            }


            MouseArea{
                id:mouseControl

                anchors.fill: imageScreen
                acceptedButtons: Qt.LeftButton | Qt.RightButton
//La corrispondenza dei bottoni tra qt e vnc è diversa
//left coincide con Qt.LeftButton, right coincide con Qt.MiddleButton, center coincide con Qt.RightButton
                onPositionChanged: {
                    console.log("movement")
                    console.log("mouse.x "+mouse.x+" mouse.y "+mouse.y+" currentScale "+screenPinch.currentScale+" mouse.buttons "+mouse.buttons)
                    if(mouseLeftAction.checked){
                        screenInterface.vncMouseEvent(mouse.x/screenPinch.currentScale,mouse.y/screenPinch.currentScale,mouse.buttons)
                    }
                }
                onPressed:  {
                    //                    console.log("pressed on screen"+flickableScreen.contentWidth+"x"+flickableScreen.contentHeight)
                    console.log("mouse.x "+mouse.x+" mouse.y "+mouse.y+" currentScale "+screenPinch.currentScale+" mouse.buttons "+mouse.buttons)
                    if(mouseLeftAction.checked){
                        if(mouse.button==Qt.LeftButton) screenInterface.vncMouseEvent(mouse.x/screenPinch.currentScale,mouse.y/screenPinch.currentScale,mouse.buttons)
                    }
                    else if(mouseRightAction.checked){
                        if(mouse.button==Qt.RightButton || mouse.button==Qt.LeftButton) screenInterface.vncMouseEvent(mouse.x/screenPinch.currentScale,mouse.y/screenPinch.currentScale,Qt.MiddleButton)
                    }
                }
                onReleased: {
                    console.log("released")
                    // il rilascio sia che si tratti di bottone destro che di bottone sinistro vuole 0 ossia Qt.NoButton
                    console.log("mouse.x "+mouse.x+" mouse.y "+mouse.y+" currentScale "+screenPinch.currentScale+" mouse.buttons "+mouse.buttons)
                    screenInterface.vncMouseEvent(mouse.x/screenPinch.currentScale,mouse.y/screenPinch.currentScale,mouse.buttons)

                }
                onPressAndHold:  {
                    console.log("pressandhold")
                    console.log("mouse.x "+mouse.x+" mouse.y "+mouse.y+" currentScale "+screenPinch.currentScale+" mouse.buttons "+mouse.buttons)
                    if(mouseLeftAction.checked || mouseRightAction.checked){
                        //Il tasto destro corrisponde a Qt.MiddleButton
                        screenInterface.vncMouseEvent(mouse.x/screenPinch.currentScale,mouse.y/screenPinch.currentScale,Qt.RightButton)
                    }
                }

            }

            onPinchStarted: {
                console.log("pinchstarted pinchscale "+pinch.scale+"currentscale "+screenPinch.currentScale)
                initialWidth = imageScreen.width
                initialHeight = imageScreen.height

            }

            onPinchUpdated: {
                console.log("onPinchUpdated prev center"+pinch.previousCenter.x +"center"+ pinch.center.x+"scale"+pinch.scale)
                console.log("x: "+initialWidth * pinch.scale)
                console.log("y: "+initialHeight * pinch.scale)
                console.log("center: "+pinch.center)
                // ref http://doc.qt.io/qt-5/qtquick-touchinteraction-pincharea-flickresize-qml.html
                //non viene impostato il contentX e contentY perchè altrimenti al resize viene spostata la posizione del riquadro
                flickableScreen.resizeContent(initialWidth * pinch.scale, initialHeight * pinch.scale, pinch.center)

            }

            onPinchFinished: {
                console.log("onPinchFinished")
                screenPinch.currentScale=screenPinch.currentScale*pinch.scale

                // Move its content within bounds.
            }

            TextField{
                id:keyboardText
                visible: false
                property int indexKey: 0
                property int textSize:  0
                property  int keyCode: 0
                //Importante per non far comparire shift automatici e dover applicare tecniche che possan generare confusione
                //Evita anche che vengano utilizzati degli anticipatori di testo che in tal caso sono fuorvianti perchè i caratteri
                //vanno passati al vnc
                echoMode: TextInput.NoEcho
                onActiveFocusChanged: {
                    if(!activeFocus){
                        keyboardAction.checked=false
                    }
                }

                onFocusChanged: {
                    console.log("keyboard text focus changed "+focus)
                    //                    focus ? keyboardButton.highlighted=true : keyboardButton.highlighted=false
                }

                Keys.onPressed: {
                    console.log("Key pressed text"+event.text+" key "+event.key)
                    screenInterface.vncKeyEvent(event.key,event.modifiers,true)
                }
                Keys.onReleased: {
                    console.log("Key released text"+event.text+" key "+event.key)
                    //Condizione di if perchè il backspace genera solo evento di relaised senza pressed
                    if(event.key === Qt.Key_Backspace)
                    {
//                        screenInterface.vncKeyEvent(event.key,event.modifiers,true)
                    }
                    if(event.key === Qt.Key_Return){
                    //viene disabilitato il bottone tastiera perchè automaticamente è come se venisse chiuso
                    //dal keyboardText
                        console.log("keyboardText force ")
                        keyboardAction.checked=false
                    }

                    screenInterface.vncKeyEvent(event.key,event.modifiers,false)

                }
                //                EnterKey.onClicked: {
                //                    //Quando si preme enter il campo non perde focus ma comunque scompare la tastiera quindi il dock rimane nascosto
                //                    //si deve pertanto forzare la ricomparsa del dockTools
                //                    console.log("Returnc pressed")
                //                    dockTools.focus=true
                //                }

                onTextChanged: {

                    keyCode=text.charCodeAt(text.length-1)
                    console.log(text.charAt(text.length-1))
                    console.log("text "+keyboardText.text+" char "+text.charAt(text.length-1)+" CHARCODE "+keyCode+" textlength"+text.length+"textsize"+textSize+"extremous"+Qt.Key_A+" "+Qt.Key_Z)
                    if(text.length>textSize)
                    {
                        console.log("screenInterface.vnckey "+text.charAt(text.length-1))
                        screenInterface.vncKey(text.charAt(text.length-1))

                    }
                    textSize=text.length
                }

            }
        }


    }
    actions{
        main: Kirigami.Action {
            id: keyboardAction
            icon.name: "input-keyboard-virtual"
            enabled: false
            checkable: true
            text: i18n("Keyboard")
            onCheckedChanged: {
                if(checked){
                    console.log("keyboard")
                    keyboardText.forceActiveFocus()
                }
		else{
                    imageScreen.forceActiveFocus()
		}
            }
        }
        left: Kirigami.Action {
            id: mouseLeftAction
            icon.name: "input-mouse-click-left"
            enabled: false
            checkable: true
            text: i18n("Mouse left")
            onTriggered: {
                if(mouseRightAction.checked==true){
                    mouseRightAction.checked=false
                }
            }
        }
        right: Kirigami.Action {
            id: mouseRightAction
            icon.name: "input-mouse-click-right"
            enabled: false
            checkable: true
            text: i18n("Mouse right")
            onTriggered: {
                if(mouseLeftAction.checked==true){
                    mouseLeftAction.checked=false
                }
            }
        }
    }


}





