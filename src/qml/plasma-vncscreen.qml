/*
VNC Screen: Remote desktop application for Kirigami KDE based on VNC standard.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.6
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import "pages"
import org.kde.kirigami 2.8 as Kirigami
import plasma.vncscreen.About 1.0
Kirigami.ApplicationWindow {
    // ID provides unique identifier to reference this element
    id: root
    property string previewSource
    // Window title
    // i18nc is useful for adding context for translators, also lets strings be changed for different languages
    title: i18nc("@title:window", "Vnc Screen")
    Component{
        id: aboutPage
        Kirigami.AboutPage {
            aboutData: AboutType.aboutData
        }
    }
    // Initial page to be loaded on app load
    pageStack.initialPage:
        MainPage {
            id: mainPage
            onVncPreviewUpdate: previewSource=source
        }
    globalDrawer: Kirigami.GlobalDrawer {
        id: actionsGlobalDrawer
        // Makes drawer a small menu rather than sliding pane
        //		isMenu: true
        actions: [
            Kirigami.Action {
                id: actionConnect
                text: i18n("Connect")
                iconName: "network-connect"
                shortcut: StandardKey.New
                onTriggered: mainPage.connectScreen(true)
                enabled: mainPage.status===MainPage.Status.Disconnected
            },
            Kirigami.Action {
                id: actionDisconnect
                text: i18n("Disconnect")
                iconName: "network-disconnect"
                shortcut: StandardKey.Close
                onTriggered: mainPage.connectScreen(false)
                enabled: mainPage.status===MainPage.Status.Connected
            },
            Kirigami.Action {
                id: actionScreenshot
                text: i18n("Screenshot")
                iconName: "preferences-system-windows-effect-screenshot"
                onTriggered: mainPage.screenshot()
                enabled: mainPage.status===MainPage.Status.Connected
            },
//            Kirigami.Action {
//                id: actionSeparator
//                separator: true
//            },
            Kirigami.Action {
                text: i18n("About")
                iconName: "help-about"
                onTriggered: pageStack.layers.push(aboutPage)
            },
            Kirigami.Action {
                id: actionQuit
                text: i18n("Quit")
                iconName: "application-exit"
                shortcut: StandardKey.Quit
                onTriggered: Qt.quit()
            }

        ]
    }


}


