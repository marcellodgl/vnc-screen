#ifndef INTERFACESETTINGS_H
#define INTERFACESETTINGS_H
#include <QSettings>
#include <QDebug>
#include <QtQuick>
class InterfaceSettings: public QQuickItem
{
    Q_OBJECT
public:
    explicit InterfaceSettings(QQuickItem *parent = 0);
    Q_INVOKABLE QVariant value(QString key);
signals:

public slots:
    void setValue(QString key,QVariant value);
private:
    QSettings *settings;
};

#endif // INTERFACESETTINGS_H
